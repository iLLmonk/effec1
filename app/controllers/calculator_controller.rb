class CalculatorController < ApplicationController

  def index
    if request.post?
      @pages_per_visit = params[:ppv].to_i
      @think_time = params[:ttime].to_i
      @page_response_time = params[:prt].to_i
      @usr_per_second = params[:ups].to_i


      @visit_duration = @pages_per_visit.to_f * (@think_time.to_f + @page_response_time.to_f)
      @active_visitors = @visit_duration * @usr_per_second.to_f
      @concurrent_page_requests = @active_visitors * (@page_response_time.to_f / (@page_response_time.to_f + @think_time.to_f))
      @throughput = (@concurrent_page_requests / @page_response_time)

    end

    render :index
  end


end
