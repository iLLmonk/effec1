$(function(){



	$.validator.methods.greater = function(value, element, param) {
		return value > param;
	};

    $('#stats-form input[type=text]').keyup(function(e){
        var input = $(this);
        input.val(input.val().replace(/[^\d]+/, ''));
    });

    $('#stats-form').validate({
        'errorClass':'text-error',
         rules:{
            'ppv':{
                'required':true,
                'number':true,
                'greater':0
            },
            'ttime':{
                'required':true,
                'number':true,
                'greater':0
            },
            'prt':{
                'required':true,
                'number':true,
                'greater':0
            },
            'ups':{
                'required':true,
                'number':true,
                'greater':0
            }
         },
        messages: {
            'ppv':'Please enter a valid number',
            'ttime':'Please enter a valid number',
            'prt':'Please enter a valid number',
            'ups':'Please enter a valid number'
        }
    });
});